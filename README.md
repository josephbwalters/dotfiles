# dotfiles
Each folder has a README and/or a script that outlines what to do.

# Software

## Software Managers
I download all of my software using these unless its unavailable.

- MacOS: [Homebrew](https://brew.sh/)
- Linux: [Homebrew](https://brew.sh/) or default package manager for the distro
- Windows: [Chocolatey](https://chocolatey.org/)

## Browser
- [Brave](https://brave.com/)

## Terminals
- MacOS: [iterm2](https://iterm2.com/)
- Windows: [Windows Terminal]()

## Shell Customization (Unix Only)
Shell: [Zsh](http://zsh.sourceforge.net/) with [Oh My Zsh](https://ohmyz.sh/) and [Powerlevel10k](https://github.com/romkatv/powerlevel10k)

Favorite Colorscheme: [Gruvbox](https://github.com/morhetz/gruvbox) - I literally use this in everything

## Software Version Manager
- Node: [nvm](https://github.com/nvm-sh/nvm)
- Ruby: [rbenv](https://github.com/rbenv/rbenv)
- Python: [pyenv](https://github.com/pyenv/pyenv) + pyenv-win

## IDEs and Editors:
1. [VSCode](https://code.visualstudio.com/) - My goto GUI editor
1. [NeoVim](https://neovim.io/) - configured initially with [vim-bootstrap](https://github.com/editor-bootstrap/vim-bootstrap) and tweaked to my liking
1. [JetBrains IDEs](https://www.jetbrains.com/) - If I want an IDE for some reason these are the best IMO
   NOTE: I manage JetBrains IDEs through the [JetBrains Toolbox](https://www.jetbrains.com/toolbox/app/) after installing it through brew

## Version Control Platforms (I only use git):
- [Gitlab](https://about.gitlab.com/) - Really good CI/CD
- [Github](https://github.com/) - They are catching up to Gitlab

# Other
## Useful Software:
- [tldr](https://tldr.sh/) - man pages but wayyyy better
- [postman](https://www.postman.com/) - API Manager
- [FZF](https://github.com/junegunn/fzf) - fuzzy find, basically necessary
- [The Silver Searcher](https://github.com/ggreer/the_silver_searcher)
- [Docker](https://www.docker.com/) - Containers are magic
- [thefuck](https://github.com/nvbn/thefuck) - this is hilarious and actually really useful
- [SwaggerDocs](https://swagger.io/docs/) - Cuz API docs are important
- [TaskWarrior](https://github.com/GothenburgBitFactory/taskwarrior)
- [TimeWarrior](https://github.com/GothenburgBitFactory/timewarrior)
- [VIT](https://github.com/scottkosty/vit)
- [TaskWarrior Pomodoro](https://github.com/coddingtonbear/taskwarrior-pomodoro)
- [Task-Time Hook](https://github.com/kostajh/taskwarrior-time-tracking-hook)
- [tig](https://github.com/jonas/tig) - ez git history
- [terjira](https://github.com/keepcosmos/terjira)
- [autojump](https://github.com/wting/autojump) - very useful
- [is-up](https://github.com/sindresorhus/is-up-cli)
- [Task Vim](https://github.com/framallo/taskwarrior.vim) - if I need it
- [VimWiki](https://github.com/vimwiki/vimwiki) - if I want it
- [TaskVimWiki](https://github.com/teranex/vimwiki-tasks) - if I want it

## Useful Links:
- [Regex Tester](https://regexr.com/) - cuz regex is hard
- [Devhints.io](https://devhints.io/) - An amazing cheatsheet website
- [Fowler Design Patterns](https://martinfowler.com/eaaCatalog/index.html) - Design Patterns will save your life
- [Hipsum](https://hipsum.co/) - generates ipsum nicely
- [Cucumber](https://cucumber.io/) - BDD is important kiddos
- [Smell -> Refactorings](https://www.industriallogic.com/wp-content/uploads/2005/09/smellstorefactorings.pdf) - Nice little guide for refactorings
- [VimAwesome](https://vimawesome.com/) - Useful vim stuff
- ... more OTW
